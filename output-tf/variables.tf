variable "container_name" {
  description = "Value of the name for the Docker container"
  # basic types include string, number and bool
  type    = string
  default = "ExampleNginxContainer"
  }

# declare that myvar exists
# but initialization nothing about it ...
# variable "myvar" {}

