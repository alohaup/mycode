terraform {
  cloud {
    organization = "bptraining"

    workspaces {
      name = "my-example"
    }
  }
}
