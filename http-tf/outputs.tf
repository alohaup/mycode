# produces an output value named "pikachu"
output "pikachu" {
  description = "API that documents pikachu"
  value       = data.http.poke.response_body
}

# produces legal JSON output value named "pikachu_json"
output "pikachu_json" {
  description = "API that documents pikachu api"
  value       = jsondecode(data.http.poke.response_body)    // note the jsondecode()
}    

